# Distant Fixes: Uvirith's Manor

Prevents distant things from popping out and in at various points in the story. **Only for OpenMW 0.47.0 or newer!**

## About

OpenMW's object paging system is great but it also exposes a detail of how the game is implented: that being a cell's scripts won't run until the cell is loaded. That means if a structure gets added as part of the story but at first does not exist, you'll get an awkward pop out as you approach the cell and it actually disables as intended when the cell loads. The reverse can happen as well; when the structure is built, it will pop in as you approach just as it popped out before.

The [Dynamic Distant Buildings for OpenMW](https://www.nexusmods.com/morrowind/mods/51236) mod by Hemaris fixes this for the Great House content as well as Ghostfence in the base Morrowind game, but there are several mods that add structures and suffer from this effect.

This mod fixes the issue for [Uvirith's Manor](https://www.nexusmods.com/morrowind/mods/44135), it requires the Telvanni module from [Dynamic Distant Buildings for OpenMW](https://www.nexusmods.com/morrowind/mods/51236).

#### Credits

**Author**: johnnyhostile

**Special Thanks**:

* Benjamin Winger for making [Delta Plugin](https://gitlab.com/bmwinger/delta-plugin/)
* Hemaris for making [Dynamic Distant Buildings for OpenMW](https://www.nexusmods.com/morrowind/mods/51236) and inspring me to make this
* KJS94 for making [Uvirith's Manor](https://www.nexusmods.com/morrowind/mods/44135)
* The OpenMW team, including every contributor (for making OpenMW and OpenMW-CS)
* All the users in the `modding-openmw-dot-com` Discord channel on the OpenMW server for their dilligent testing <3
* Bethesda for making Morrowind

And a big thanks to the entire OpenMW and Morrowind modding communities! I wouldn't be doing this without all of you.

#### Web

[Project Home](https://modding-openmw.gitlab.io/distant-fixes-uviriths-manor/)

<!--[Nexus Mods](https://www.nexusmods.com/morrowind/mods/TODO)-->

[Source on GitLab](https://gitlab.com/modding-openmw/distant-fixes-uviriths-manor)

#### Installation

**This mod requires OpenMW 0.47.0 or newer!**

[This short video](https://www.youtube.com/watch?v=xzq_ksVuRgc) demonstrates the whole process in under two minutes.

1. Download the zip from a link above
1. Extract the contents to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\DistantFixes\distant-fixes-uviriths-manor

        # Linux
        /home/username/games/OpenMWMods/DistantFixes/distant-fixes-uviriths-manor

        # macOS
        /Users/username/games/OpenMWMods/DistantFixes/distant-fixes-uviriths-manor
1. Add the appropriate data path to your `openmw.cfg` file, for example:

        data="C:\games\OpenMWMods\DistantFixes\distant-fixes-uviriths-manor"
1. Enable the mod plugin via OpenMW-Launcher, or add this to `openmw.cfg` ([official OpenMW documentation](https://openmw.readthedocs.io/en/latest/reference/modding/mod-install.html#install)):

        content=DistantFixesUvirithsManor.esp
1. You should load this plugin right after `HM_DDD_Strongholds_T_v1.0.esp` (from [Dynamic Distant Buildings for OpenMW](https://www.nexusmods.com/morrowind/mods/51236)).

#### Connect

* [Discord](https://discord.gg/KYKEUxFUsZ)
* [IRC](https://web.libera.chat/?channels=#momw)
* File an issue [on GitLab](https://gitlab.com/modding-openmw/distant-fixes-uviriths-manor/-/issues/new) for bug reports or feature requests
* Email: `johnnyhostile at modding-openmw dot com`
<!--* Leave a comment [on Nexus mods](https://www.nexusmods.com/morrowind/mods/TODO?tab=posts)-->
