## Distant Fixes: Uvirith's Manor Changelog

#### 1.2

* Optimized scripts.

[Download Link](https://gitlab.com/modding-openmw/distant-fixes-uviriths-manor/-/packages/30755401) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/53140)

#### 1.1

* Fixed incorrect usage of `StartScript` which would cause major FPS issues.

[Download Link](https://gitlab.com/modding-openmw/distant-fixes-uviriths-manor/-/packages/30744654) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/53140)

#### 1.0

* Initial release of the mod.

[Download Link](https://gitlab.com/modding-openmw/distant-fixes-uviriths-manor/-/packages/15708214) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/53140)
